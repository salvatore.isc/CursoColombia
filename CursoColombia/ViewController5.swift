//
//  ViewController5.swift
//  CursoColombia
//
//  Created by Salvador Lopez on 24/05/23.
//

import UIKit

class ViewController5: UIViewController {
    
    var options = ["Leche","Huevo","Harina","Miel","Azucar"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //self.view.backgroundColor = .lightGray
        
        //MARK: PICKER VIEW
        let myPickerView = UIPickerView()
        myPickerView.frame = CGRect(x: 0, y: 100, width: Constantes.wScreen, height: 260)
        myPickerView.delegate = self
        myPickerView.dataSource = self
        self.view.addSubview(myPickerView)
    }

}

extension ViewController5: UIPickerViewDelegate {
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return options[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        print("Se selecciono la celda: \(row) del componente: \(component)")
        print("Corresponde a la opcion: \(options[row])")
    }
    
}

extension ViewController5: UIPickerViewDataSource {
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return 5
    }
    
    
}

