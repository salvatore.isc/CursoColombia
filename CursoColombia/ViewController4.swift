//
//  ViewController4.swift
//  CursoColombia
//
//  Created by Salvador Lopez on 23/05/23.
//

import UIKit

class ViewController4: UIViewController {
    
    var seconds: Float = 0
    var timer: Timer!
    var myProgressView: UIProgressView!
    
    var textLabel = UILabel()

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        self.view.addSubview(textLabel)
        
        // Calling UISetUp VC4
        initUI()
        // Calling UIProgressView
        //crearProgressView()
    }
    
    func initUI (){
        
        // MARK: UISlider
        let slider = UISlider(frame: CGRect(x: 15, y: 410, width: 180, height: 20))
        slider.minimumValue = 0
        slider.maximumValue = 50
        slider.isContinuous = true
        slider.tintColor = UIColor.green
        slider.value = 25
        slider.addTarget(self, action: #selector(sliderUpdateValue(sender:)), for: .valueChanged)
        //self.view.addSubview(slider)
        
        //MARK: UISwitch
        let mySwitch = UISwitch()
        mySwitch.frame.origin = CGPoint(x: 15, y: 350)
        mySwitch.isOn = true
        
        // Delay
        DispatchQueue.main.asyncAfter(deadline: .now() + 2){
            mySwitch.setOn(false, animated: true)
        }
        
        mySwitch.addTarget(self, action: #selector(switchUpdateValue(sender:)), for: .valueChanged)
        //self.view.addSubview(mySwitch)
        
        //MARK: UIActivityIndicator
        let myActivityIndicator = UIActivityIndicatorView()
        myActivityIndicator.style = .medium
        myActivityIndicator.center = self.view.center
        myActivityIndicator.startAnimating()
        myActivityIndicator.hidesWhenStopped = true
        DispatchQueue.main.asyncAfter(deadline: .now() + 2){
            myActivityIndicator.stopAnimating()
        }
        //self.view.addSubview(myActivityIndicator)
        
        //MARK: UIStepper
        let myStepper = UIStepper()
        myStepper.frame.origin = CGPoint(x:15, y:400)
        myStepper.maximumValue = 1
        myStepper.minimumValue = -1
        myStepper.stepValue = 0.1
        myStepper.value = -1
        //myStepper.autorepeat = false
        myStepper.wraps = true
        myStepper.addTarget(self, action: #selector(stepperUpdateValue(sender:)), for: .valueChanged)
        //self.view.addSubview(myStepper)
        
        //MARK: UIDatePicker
        let datePicker = UIDatePicker()
        datePicker.center = self.view.center
        datePicker.datePickerMode = .date
        datePicker.backgroundColor = .lightGray
        datePicker.preferredDatePickerStyle = .compact
        datePicker.addTarget(self, action: #selector(datePickerSelected(sender:)), for: .valueChanged)
        self.view.addSubview(datePicker)
        
        
    }
    
    @objc func datePickerSelected(sender: UIDatePicker){
        print(sender.date)
        //MARK: Formato de fecha
        let format = DateFormatter()
        format.dateFormat = "EEE, MMM d ''yy"
        let dateFormated = format.string(from: sender.date)
        print(dateFormated)
    }
    
    @objc func stepperUpdateValue(sender: UIStepper){
        print("El valor actual es: \(sender.value)")
    }
    
    @objc func sliderUpdateValue(sender:UISlider) {
        self.textLabel.text = "El valor del slider es: \(sender.value)"
    }
    
    @objc func switchUpdateValue(sender: UISwitch){
        print(sender.isOn)
    }
    
    //MARK: UIProgressView
    
    func crearProgressView(){
        myProgressView = UIProgressView(progressViewStyle: .default)
        myProgressView.frame.size.width = 300
        myProgressView.center = self.view.center
        myProgressView.progress = 0
        myProgressView.trackTintColor = .gray
        myProgressView.progressTintColor = UIColor.orange
        //self.view.addSubview(myProgressView)
        //exTimer()
    }
    
    //MARK: EXECUTE TIMER
    
    func exTimer(){
        timer = Timer.scheduledTimer(timeInterval: 0.3, target: self, selector: #selector(updateProgressView), userInfo: nil, repeats: true)
    }
    
    //MARK: UPDATE PROGRESS VIEW
    @objc func updateProgressView(){
        seconds += 1
        if seconds <= 10 {
            myProgressView.setProgress(seconds/10, animated: true)
            print("Porcentaje: \(seconds*10)%")
        }else{
            timer.invalidate()
        }
    }
    
}
