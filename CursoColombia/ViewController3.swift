//
//  ViewController3.swift
//  CursoColombia
//
//  Created by Salvador Lopez on 19/05/23.
//

import UIKit

class ViewController3: UIViewController {

    @IBOutlet weak var correoElectronicoLb: UITextField!
    @IBOutlet weak var contraseniaLb: UITextField!
    
    @IBOutlet weak var btnIniciaSession: UIButton!
    
    @IBAction func btnIniciaSessionAction(_ sender: Any) {
        if correoElectronicoLb.text != "" && contraseniaLb.text != ""{
            print("Usuario: \(correoElectronicoLb.text!)")
            print("Contrasenia: \(contraseniaLb.text!)")
        }
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        //OUTLETS
        btnIniciaSession.tintColor = .white
        btnIniciaSession.backgroundColor = .lightGray
        //btnIniciaSession.isEnabled = true
        
        initUI()
    }
    
    func initUI(){
        
        //MARK: UILABEL
        let label = UILabel(frame: CGRect(x: (Constantes.wScreen/2)-100, y: (Constantes.hScreen/2)-25, width: 200, height: 50))
        label.textAlignment = NSTextAlignment.center
        label.text = "Nombre de usuario:"
        label.layer.borderWidth = 1.0
        label.textColor = UIColor.blue
        //self.view.addSubview(label)
        
        //MARK: UIButton
        let button = UIButton()
        button.frame = CGRect(origin: CGPoint(x: 100, y: 100), size: CGSize(width: 90, height: 50))
        button.backgroundColor = .purple
        button.setTitle("Click me!", for: .normal)
        //Action
        button.addTarget(self, action: #selector(clickMeAction), for: .touchUpInside)
        button.isEnabled = false
        //button.isHidden = true
        //self.view.addSubview(button)
        
        //MARK: UITextField
        let textField = UITextField()
        textField.frame.origin = CGPoint(x: 100, y: 200)
        textField.frame.size = CGSize(width: 200, height: 50)
        textField.placeholder = "Coloca aqui tu nombre"
        textField.textColor = .blue
        textField.textAlignment = .center
        textField.borderStyle = .line
        textField.layer.borderWidth = 1
        //textField.layer.borderColor = CGColor.init(red: 51/255, green: 51/255, blue: 255/255, alpha: 0.8)
        //textField.layer.borderColor = UIColor.red.cgColor
        //self.view.addSubview(textField)
        
        //MARK: UIImageView
        let imgName = "tux"
        let img = UIImage(named: imgName)
        let imgView = UIImageView(image: img!)
        imgView.contentMode = .scaleToFill
        imgView.frame = CGRect(x: 100, y: 200, width: 200, height: 100)
        //self.view.addSubview(imgView)
        
        //MARK: UITextView
        let str: NSString = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean gravida arcu ut justo tristique condimentum. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Curabitur mollis leo magna. Maecenas elementum urna volutpat nisl interdum laoreet. Integer convallis, nisl at aliquam lacinia, augue mauris pulvinar nunc, vitae hendrerit justo diam eget ipsum. Donec porttitor sapien et nunc blandit, finibus blandit sem malesuada. Donec gravida blandit accumsan."
        let textView = UITextView(frame: CGRect(x: 15, y: 280, width: 350, height: 70))
        textView.text = str as String
        textView.isEditable = false
        textView.isSelectable = false
        //self.view.addSubview(textView)
        
        //MARK: UITextView 2
        let myBoldWord = str.range(of: "ipsum")
        
        let myMutableString = NSMutableAttributedString(string: str as String, attributes: [NSAttributedString.Key.font:UIFont(name: "Georgia", size: 18)!])
        
        myMutableString.addAttribute(NSAttributedString.Key.font, value: UIFont(name: "Helvetica Neue", size: 36)!, range: myBoldWord)
        
        let textView2 = UITextView(frame: CGRect(x: 15, y: 350, width: 350, height: 70))
        textView2.attributedText = myMutableString
        
        //self.view.addSubview(textView2)
        
        
    }
    
    @objc func clickMeAction(){
        print("Ouch!!")
    }
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
        let etiqueta = UILabel(frame: CGRect(x: 10, y: 20, width: 200, height: 40))
        etiqueta.text = "Fui creado desde el vc1"
        etiqueta.textColor = UIColor.red
        if(segue.identifier == "mySegue"){
            let vc = segue.destination as! ViewController4
            vc.textLabel = etiqueta
        }
    }

}
