//
//  Constantes.swift
//  CursoColombia
//
//  Created by Salvador Lopez on 19/05/23.
//

import UIKit

struct Constantes {
    static let wScreen = UIScreen.main.bounds.width
    static let hScreen = UIScreen.main.bounds.height
}
