//
//  ViewController.swift
//  CursoColombia
//
//  Created by Salvador Lopez on 19/05/23.
//

import UIKit

/**
 
 class User(){
   var email: String
   var pass: String
   var activo: Bool = false
 }
 
 */

// var usuario: User()

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        print("El VC cargó la vista.") // 1
        // consumir un ws de lista de usarios
    }
    
    override func viewWillAppear(_ animated: Bool) {
        print("La vista del VC va a aparecer.") // 2
    }
    
    override func viewDidAppear(_ animated: Bool) {
        print("La vista del VC apareció.") //3
        
        // Recargar la info del usuario
        // Actualizar el select con el valor de la propiedad del obj.
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        print("La vista del VC va a desaparecer.")
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        print("La vista del VC a desaparecido.")
    }
    
    // Metodo para preparar para transicion [prepareforsegue]
    // Enviar el obj usuario a VC2


}

