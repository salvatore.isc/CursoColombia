//
//  ViewController6.swift
//  CursoColombia
//
//  Created by Salvador Lopez on 24/05/23.
//

import UIKit

class ViewController6: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        initScrollView()
    }
    
    func initScrollView(){
        let rect = self.view.bounds // Origen y tamanio de la vista del VC.
        let scrollView = UIScrollView(frame: rect)
        let img = UIImage(named: "landscape")
        let imgView = UIImageView(image: img)
        self.view.addSubview(scrollView)
        scrollView.addSubview(imgView)
        scrollView.contentSize = img!.size
        scrollView.contentInset = UIEdgeInsets(top: -50, left: 10, bottom: -50, right: 10)
        scrollView.contentOffset = CGPoint(x: 600, y: 900)
        scrollView.scrollIndicatorInsets = UIEdgeInsets(top: 40, left: 40, bottom: 40, right: 40)
    }

}
