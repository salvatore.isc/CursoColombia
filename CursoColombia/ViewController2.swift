//
//  ViewController2.swift
//  CursoColombia
//
//  Created by Salvador Lopez on 19/05/23.
//

import UIKit

class ViewController2: UIViewController {
    
    // var usuario: User()

    override func viewDidLoad() {
        super.viewDidLoad()
        print("El VC2 cargó la vista.") // 1
        // Recibir el usuario
        // if let usuarioSeguro = user {
            // llamar metodo para validar si esta activo.
        //}
    }
    
    override func viewWillAppear(_ animated: Bool) {
        print("La vista del VC2 va a aparecer.") // 2
    }
    
    override func viewDidAppear(_ animated: Bool) {
        print("La vista del VC2 apareció.") //3
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        print("La vista del VC2 va a desaparecer.")
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        print("La vista del VC2 a desaparecido.")
    }
    
    // func validar usuario activo --> user con nuevos valores en activo.

}
