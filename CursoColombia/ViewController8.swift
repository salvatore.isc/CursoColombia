//
//  ViewController8.swift
//  CursoColombia
//
//  Created by Salvador Lopez on 25/05/23.
//

import UIKit

class ViewController8: UIViewController {

    @IBOutlet weak var welcomeLb: UILabel!
    @IBOutlet weak var userTexField: UITextField!
    @IBOutlet weak var passTextField: UITextField!
    @IBOutlet weak var loginBtn: UIButton!
    @IBOutlet weak var imgTux: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        imgTux.alpha = 1
        loginBtn.alpha = 0
        welcomeLb.alpha = 0
        loginBtn.tintColor = .gray
    }
    
    override func viewWillAppear(_ animated: Bool) {
        UIView.animate(withDuration: 5, delay: 1) {
            self.imgTux.isHidden = false
            self.imgTux.alpha = 0.4
        }
        UIView.animate(withDuration: 2.5) {
            self.imgTux.layer.frame.origin.y = 520
            self.imgTux.layer.frame.origin.x = 137
        }
        UIView.animate(withDuration: 3.5, delay: 0.5, animations: {
            self.imgTux.layer.frame.size.width = 480
            self.imgTux.layer.frame.size.height = 610
            self.view.backgroundColor = UIColor.lightGray
            self.userTexField.layer.frame.origin.x = 68
            self.passTextField.layer.frame.origin.x = 68
        }, completion: { _ in
            self.loginBtn.alpha = 1
            self.welcomeLb.alpha = 1
        })
    }

}
