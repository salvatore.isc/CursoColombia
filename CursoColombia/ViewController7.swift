//
//  ViewController7.swift
//  CursoColombia
//
//  Created by Salvador Lopez on 24/05/23.
//

import UIKit

class ViewController7: UIViewController {

    var nombres = ["Expresso", "Expresso Doble", "Expresso Cortado", "Expresso con Crema"]
    var precios = [10,11,11,12]
    var promos = [true, false, false, true]
    
    
    @IBOutlet weak var myTableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //myTableView.dataSource = self
    }

}

extension ViewController7: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return nombres.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        //let cell = UITableViewCell(style: .subtitle, reuseIdentifier: "celda")
        //cell.textLabel?.text = "Celda: \(nombres[indexPath.row])"
        //cell.detailTextLabel?.text = "Seccion: \(indexPath.section)"
        //return cell
        let cell2 = myTableView.dequeueReusableCell(withIdentifier: "myCell", for: indexPath)
        cell2.textLabel?.text = nombres[indexPath.row]
        cell2.detailTextLabel?.text = "\(precios[indexPath.row])"
        if promos[indexPath.row] {
            cell2.backgroundColor = .gray
        }
        return cell2
    }
    
}

extension ViewController7: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 120
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print("El precio del producto: \(nombres[indexPath.row]), es: \(precios[indexPath.row])")
        
        if promos[indexPath.row] {
            //MARK: ALERT CONTROLLER
            let alertController = UIAlertController(title: "¡Promoción de esta semana!", message: "Compra dos \(nombres[indexPath.row]) y te regalamos el tercero ¡Gratis!", preferredStyle: .alert)
            
            let alertOkay = UIAlertAction(title: "Aceptar", style: .default){ _ in
                print("El usuario agrego otro \(self.nombres[indexPath.row]), para llevarse un tercer \(self.nombres[indexPath.row]) gratis.")
            }
            let alertCancel = UIAlertAction(title: "No me interesa, garcias", style: .destructive){ _ in
                print("Agregar solo un \(self.nombres[indexPath.row])")
            }
            
            let alert3 = UIAlertAction(title: "Otra opción...", style: .default)
            
            alertController.addAction(alertOkay)
            alertController.addAction(alert3)
            alertController.addAction(alertCancel)
            
            /*alertController.addTextField{ (textField) in
                textField.placeholder = "Ingresa un codigo de promoción"
            }*/
            
            self.present(alertController, animated: true)
        }
        
    }
    
}
